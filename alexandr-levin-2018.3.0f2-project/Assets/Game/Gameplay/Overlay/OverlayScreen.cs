using UnityEngine;
using UnityEngine.UI;
using Zenject;
using Screen = Game.Base.Screen;

namespace Game.Gameplay.Overlay
{
    public class OverlayScreen : Screen
    {
        public int Score { get; set; }
        public int TimeLeftSeconds { get; set; }

        public Text TimerText;
        public Text ScoreText;

        public Text FinalScoreText;
        public Button MenuButton;

        private Router _router;
        private GameSession _session;
        private Animator _animator;

        [Inject]
        private void InjectDependencies(Router router, GameSession gameSession)
        {
            _router = router;
            _session = gameSession;
        }

        private void Awake()
        {
            _animator = GetComponent<Animator>();
            
            _session.OnTimerProgressChanged += _ => UpdateTimerText(_session.TimeLeftSeconds);
            _session.OnScoreChanged += UpdateScoreText;
            _session.OnFinish += () => SetFinishState();
            
            MenuButton.onClick.AddListener(() => { SetFadeOutState(); });
        }

        private void Start()
        {
            UpdateTimerText(_session.TimeLeftSeconds);
            UpdateScoreText(_session.Score);
        }

        private void UpdateTimerText(int secondsLeft)
        {
            TimerText.text = secondsLeft.ToString();
        }

        private void UpdateScoreText(int score)
        {
            ScoreText.text = score.ToString();
        }

        private void SetFinishState()
        {
            FinalScoreText.text = _session.Score.ToString();
            _animator.SetTrigger("Finish");
        }

        private void SetFadeOutState()
        {
            _animator.SetTrigger("Fade_Out");
        }

        // called from Animation by Animation Event
        private void FadeOutAnimationFinished()
        {
            _router.ResetCurrentScene();
        }
        
    }
}